#ifndef __MPU6050_H__
#define __MPU6050_H__

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_i2c.h"

#define   MPU6050_ADDRESS   0x0D  	//(0x68 << 1)
#define   SMPLRT_DIV      	0x19   	//0x07(125Hz)
#define   CONFIG        		0x1A   		//0x06(5Hz)
#define   GYRO_CONFIG      	0x1B   
#define   ACCEL_CONFIG  		0x1C   
#define   ACCEL_XOUT_H   		0x3B
#define   ACCEL_XOUT_L   		0x3C
#define   ACCEL_YOUT_H   		0x3D
#define   ACCEL_YOUT_L   		0x3E
#define   ACCEL_ZOUT_H   		0x3F
#define   ACCEL_ZOUT_L   		0x40
#define   TEMP_OUT_H      	0x41
#define   TEMP_OUT_L      	0x42
#define   GYRO_XOUT_H      	0x43
#define   GYRO_XOUT_L      	0x44   
#define   GYRO_YOUT_H      	0x45
#define   GYRO_YOUT_L      	0x46
#define   GYRO_ZOUT_H      	0x47
#define   GYRO_ZOUT_L      	0x48
#define   PWR_MGMT_1     		0x6B   //
#define   WHO_AM_I         	0x75   //

#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)
#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<DOR)

typedef struct
{
	float Roll;
	float Pitch;
}Angle;

void mpu6050_read(unsigned char add, uint8_t *respond, unsigned char n);
void mpu6050_getgyro(void);
void mpu6050_getgyrodata(void);
void mpu6050_getaccel(void);
void mpu6050_getacceldata(void);
void mpu6050_write(unsigned char add,unsigned char data);
void mpu6050_init(void);
void mpuStart(void);
void mpuCalib(void);
void mpuGetData(void);
void fusionGetEuler(void);
//void zeroImu(void);
void Move(uint8_t mode);
#endif

