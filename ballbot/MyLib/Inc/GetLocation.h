#ifndef LOCATION_H
#define LOCATION_H

void Zero(void);
void LocationCalculator(float _R, float _theta_a, float _theta_b, float _theta_c, double _v_a, double _v_b, double _v_c);
void LocationUpdate(float rot);
double GetRotWo(void);

#endif

