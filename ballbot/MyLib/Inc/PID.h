#ifndef __PID_H__
#define __PID_H__
#include "stm32f4xx_hal.h"
#include <math.h>
#include <stdlib.h>

#define SIGN(a) (a < 0 ? -1 : 1)

typedef struct
{
		float Kp;
		float Ki;
		float Kd;
		double pPart;
		double iPart;
		double dPart;
		double PIDResult;
		double PIDResultTemp;
		double PIDError;
		double PIDErrorTemp1;
		double PIDErrorTemp2;
} PIDType;

void PIDVx(float Angle, int MaxResponse);
void PIDVy(float Angle, int MaxResponse);
void PIDMotorA(double SetPoint, double Speed, int MaxResponse);
void PIDMotorB(double SetPoint, double Speed, int MaxResponse);
void PIDMotorC(double SetPoint, double Speed, int MaxResponse);


#endif

