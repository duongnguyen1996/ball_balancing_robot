#ifndef __UART_H__
#define __UART_H__

#include "stm32f4xx_hal.h"
#include "utils.h"
#include "PID.h"


#define UART1_RX_BUFFER_SIZE 255
#define PID_DATA 							50



void PID_Parameter(void);
void ClearUart1RxBuffer(void);
void ClearUart1RxIndex(void);



#endif
