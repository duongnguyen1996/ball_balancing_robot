#ifndef __HB_MOTOR_H_
#define __HB_MOTOR_H_
#include "stm32f4xx_hal.h"

void Start_Motor(void);
void Motor_PWM_A(int Percent);
void Motor_PWM_B(int Percent);
void Motor_PWM_C(int Percent);
void Motor_A_Run(int Power);
void Motor_B_Run(int Power);
void Motor_C_Run(int Power);
int Map(double value,int PWM);


#endif

