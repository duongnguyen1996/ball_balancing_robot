#include "GetLocation.h"
#include <math.h>

#define PI 3.1415926
#define SINDEG(a)	(sin(a*PI/180))
#define COSDEG(a)	(cos(a*PI/180))

#define _dt 0.005


extern double v_x, v_y;
extern double v_x_rel, v_y_rel;
extern double w;
    
extern double x, y;

double theta = 0; // current angle

double theta_a, theta_b, theta_c;
double v_a, v_b, v_c;
double R;

double s_a;   double c_a;
double s_b;   double c_b;
double s_c;   double c_c;

double det;

double average_theta;
double alpha;


void Zero(void)
{
	x = 0;
	y = 0;
}

void LocationCalculator(float _R, float _theta_a, float _theta_b, float _theta_c, double _v_a, double _v_b, double _v_c)
{
	R = _R;

  theta_a = _theta_a;
  theta_b = _theta_b;
  theta_c = _theta_c;

  v_a = _v_a;
  v_b = _v_b;
  v_c = _v_c;

  s_a = SINDEG(theta_a);  c_a = COSDEG(theta_a);
  s_b = SINDEG(theta_b);  c_b = COSDEG(theta_b);
  s_c = SINDEG(theta_c);  c_c = COSDEG(theta_c);

  /* matrix stuff */
  det = (c_c*s_a - c_b*s_a + c_a*s_b - c_c*s_b + c_b*s_c - c_a*s_c);
}

void LocationUpdate(float rot)
{
   v_x_rel = (c_b - c_c) * (v_a) + (c_c - c_a) * (v_b) + (c_a - c_b) * (v_c);
   v_x_rel /= det; 
	
   v_y_rel = (s_b - s_c) * (v_a) + (s_c - s_a) * (v_b) + (s_a - s_b) * (v_c);
   v_y_rel /= det;             
  
// first transform velocities to real coordinates.
   v_x = COSDEG(rot) * v_x_rel - SINDEG(rot) * v_y_rel;
   v_y = SINDEG(rot) * v_x_rel + COSDEG(rot) * v_y_rel;	
   
   // now integrate
   x += v_x * _dt / 1000;
   y += v_y * _dt / 1000;	
}

double GetRotWo(void)
{
	w = (c_b*s_c - c_c*s_b)*(v_a) + (c_c*s_a - c_a*s_c)*(v_b) + (c_a*s_b - c_b*s_a)*(v_c);
        
  w = w / det / R;  // in rad
  w = w * 180 / PI; // in deg

  // linear equations solved. Now integrate.     
  theta += w * _dt / 1000;
  if (theta >= 360) theta -= 360;

  average_theta = (theta - w * _dt / 1000) / 2;
  return (-average_theta);
}

