#include "MPU6050.h"
#include <math.h>
#include "MadgwickAHRS.h"
//#include "kalman.h"

#define PI 3.1415926f
#define Calibnumber 100
//#define dT 0.002 //Warning

extern I2C_HandleTypeDef hi2c2;
extern double wA;
extern double wB;
extern double wC;
extern int32_t pitch_en ;
extern int32_t roll_en ;
extern float move;
extern float roll_uart, pitch_uart;
//extern float pitch_uart;


uint8_t mode = 0;
int16_t mpu6050_g[3];
int16_t mpu6050_a[3];
float mpu6050_gf[3];
float mpu6050_af[3];
uint8_t mpu6050_tbuff[6];
uint8_t mpu6050_rbuff[6];
float mpu6050g_offset[3] = {0.0f, 0.0f, 0.0f};
float mpu6050a_offset[3] = {0.0f, 0.0f, 0.0f};
float roll_offset = 0.0f, pitch_offset = 0.0f;
//Angle Acc, GyroRate, AngleC;
Angle AngleM;

void mpu6050_init(void)
{
	mpu6050_write(PWR_MGMT_1, 	0x80);
	HAL_Delay(15);
	
  mpu6050_write(PWR_MGMT_1, 	0x00);  // internal 8MHz, disabled SLEEP mode, disable CYCLE mode  
  mpu6050_write(SMPLRT_DIV, 	0x03);	 //sample rate: 8khz
  mpu6050_write(CONFIG, 			0x06);		 //DLPF disable
  mpu6050_write(GYRO_CONFIG, 	0x10);  //full scale range mode 2 +-1000do/s
  mpu6050_write(ACCEL_CONFIG, 0x10); //full scale range mode 2 +-8g
}

void mpu6050_write(unsigned char add,unsigned char data)
{	
	*mpu6050_tbuff=add;
	*(mpu6050_tbuff+1)=data;
	HAL_I2C_Master_Transmit(&hi2c2,0xD0,mpu6050_tbuff,2,1);
}

void mpu6050_read(unsigned char add, uint8_t *respond, unsigned char n)
{
	*mpu6050_tbuff=add;
	HAL_I2C_Mem_Read(&hi2c2, 0xD0, add,I2C_MEMADD_SIZE_8BIT, respond, n, 1000);
}  
void mpu6050_getgyro(void)
{   
	mpu6050_read(0x43, mpu6050_rbuff, 6);
  *mpu6050_g = ((*mpu6050_rbuff)<<8)|*(mpu6050_rbuff+1); 							
  *(mpu6050_g+1) = (*(mpu6050_rbuff+2)<<8)|*(mpu6050_rbuff+3);
  *(mpu6050_g+2) = (*(mpu6050_rbuff+4)<<8)|*(mpu6050_rbuff+5);
}

void mpu6050_getgyrodata(void)
{
	mpu6050_getgyro();   
	*mpu6050_gf = *mpu6050_g/32.8 - *mpu6050g_offset;
	*(mpu6050_gf+1) = *(mpu6050_g+1)/32.8 - *(mpu6050g_offset+1);
	*(mpu6050_gf+2) = *(mpu6050_g+2)/32.8 - *(mpu6050g_offset+2);  

}

void mpu6050_getaccel(void)
{   
	mpu6050_read(0x3B, mpu6050_rbuff,6);
  *mpu6050_a = ((*mpu6050_rbuff)<<8)|*(mpu6050_rbuff+1); 
  *(mpu6050_a+1) = (*(mpu6050_rbuff+2)<<8)|*(mpu6050_rbuff+3);
  *(mpu6050_a+2) = (*(mpu6050_rbuff+4)<<8)|*(mpu6050_rbuff+5);
}

void mpu6050_getacceldata(void)
{
	mpu6050_getaccel();
	*mpu6050_af = *mpu6050_a/4096.0 - *mpu6050a_offset;
	*(mpu6050_af+1) = *(mpu6050_a+1)/4096.0 - *(mpu6050a_offset+1);
	*(mpu6050_af+2) = *(mpu6050_a+2)/4096.0 - *(mpu6050a_offset+2); 
}

void mpuStart(void)
{
  mpu6050_init();
	mpu6050_getacceldata();
	mpu6050_getgyrodata();
	mpuCalib();
	MadgwickAHRSupdateIMU(mpu6050_gf[0]*PI/180, mpu6050_gf[1]*PI/180, mpu6050_gf[2]*PI/180
												, mpu6050_af[0], mpu6050_af[1], mpu6050_af[2]);
	fusionGetEuler();
	//zeroImu();

}

void mpuCalib(void)
{
	float x_accel = 0;
	float y_accel = 0;
	float z_accel = 0;
	float x_gyro = 0;
	float y_gyro = 0;
	float z_gyro = 0;

	for(uint8_t i = 0; i < Calibnumber; i++)
	{
		mpu6050_getacceldata();
		mpu6050_getgyrodata();
		
		x_accel += mpu6050_af[0];
		y_accel += mpu6050_af[1];
		z_accel += mpu6050_af[2];
		
		x_gyro += mpu6050_gf[0];
		y_gyro += mpu6050_gf[1];
		z_gyro += mpu6050_gf[2];
		HAL_Delay(2);
	}
	
	x_accel /= Calibnumber;
	y_accel /= Calibnumber;
	z_accel /= Calibnumber;
	
	x_gyro /= Calibnumber;
	y_gyro /= Calibnumber;
	z_gyro /= Calibnumber;
	
	x_accel = mpu6050a_offset[0];
	y_accel = mpu6050a_offset[1];
	z_accel = mpu6050a_offset[2];
	
	x_gyro = mpu6050g_offset[0];
	y_gyro = mpu6050g_offset[1];
	z_gyro = mpu6050g_offset[2];
	
	//printf("End Calib ... \r\n");
}


void mpuGetData(void)
{
	mpu6050_getacceldata();
	mpu6050_getgyrodata();	
	MadgwickAHRSupdateIMU(mpu6050_gf[0]*PI/180.0f, mpu6050_gf[1]*PI/180.0f, mpu6050_gf[2]*PI/180.0f
												, mpu6050_af[0], mpu6050_af[1], mpu6050_af[2]);

}

void fusionGetEuler(void)
{
  sensfusionGetEulerRPY(&AngleM.Pitch, &AngleM.Roll);
	Move(mode);
//			if (mode == 0)
//		{
//			roll_offset =	0.0;
//			pitch_offset =	1.5;
//	}
//	if (mode == 1)
//		{
//			roll_offset =	0.5;
//			pitch_offset =	1.5;
//			
//			if (-roll_en > moveup*3500)
//     {
//	  mode = 0;
//		 }
//			
//		}
//		if (mode == 2)
//		{
//			roll_offset =	-0.5;
//			pitch_offset =	1.5;
//			
//			if (roll_en > moveup*3500)
//     {
//	  mode = 0;
//		 }
//			
//		}
//    	if (mode == 3)
//		{
//			roll_offset =	0.0;
//			pitch_offset =	2.0;
//			
//			if (-pitch_en > moveup*3500)
//     {
//	  mode = 0;
//		 }
//			
//		}
//		
//		if (mode == 4)
//		{
//			roll_offset =	0.0;
//			pitch_offset =	1.0;
//			
//			if (pitch_en > moveup*3500)
//     {
//	  mode = 0;
//		 }
//			}
	
	AngleM.Roll -= roll_offset;
	AngleM.Pitch -= pitch_offset;
	//Value not filter
//	Acc.Pitch = - atan2(mpu6050_a[1], sqrt(mpu6050_a[0]*mpu6050_a[0] + mpu6050_a[2]*mpu6050_a[2]))*180/PI; ///--
//	GyroRate.Pitch = - mpu6050_g[0]/131.0; ///--
//	Acc.Roll = atan2(mpu6050_a[0],mpu6050_a[2])*180/PI;
//	GyroRate.Roll = mpu6050_g[1]/131.0;
}

void Move(uint8_t mode)
{
	switch(mode)
	{
		case 0:
		{
			roll_offset =	roll_uart;
			pitch_offset =	pitch_uart;
			break;
		}
		case 1:
		{
			roll_offset =	roll_uart + 0.5f;
			pitch_offset =	pitch_uart;
			
			if (-roll_en > move*3500)
			{
				mode = 0;
			}
			
			break;
		}
		case 2:
		{
			roll_offset =	roll_uart - 0.4f;
			pitch_offset =	pitch_uart;
			
			if (roll_en > move*3500)
			{
				mode = 0;
			}
			break;
		}
		case 3:
		{
			roll_offset =	roll_uart;
			pitch_offset =	pitch_uart + 0.4f;
			
			if (-pitch_en > move*3500)
			{
				mode = 0;
			}
			break;
		}
		case 4:
		{
			roll_offset =	roll_uart;
			pitch_offset =	pitch_uart - 0.4f;
			
			if (pitch_en > move*3500)
			{
				mode = 0;
			}
			break;
		}
	}
}

