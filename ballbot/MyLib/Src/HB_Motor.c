#include "HB_Motor.h"

#define MaxResponse 500
uint8_t Deadband = 50;

extern TIM_HandleTypeDef htim3;
/*
		Prescaler = Ftimer / (STEP_PWM * Fpwm)
		s.t Ftimer = 84MHz (ABH1)
		
		Choose Fpwm = 14kHz to control motor (possible)
	Power 0 <--> 0%
	Power 500 	<--> 100%
*/

void Start_Motor(void)
{
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
}

void Motor_PWM_A(int Percent)
{
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, Percent);
}

void Motor_PWM_B(int Percent)
{
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, Percent);
}

void Motor_PWM_C(int Percent)
{
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, Percent);
}


void Motor_A_Run(int Power)
{
	if(Power >= MaxResponse)
		Power = MaxResponse;
	if(Power <= - MaxResponse)
		Power = - MaxResponse;
	
	if(Power >= 0)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
		Motor_PWM_A(Power);
	}
	else 
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
		Motor_PWM_A(MaxResponse-(-Power));
	}
}

void Motor_B_Run(int Power)
{
	if(Power >= MaxResponse)
		Power = MaxResponse;
	if(Power <= - MaxResponse)
		Power = - MaxResponse;
	
	if(Power >= 0)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET);
		Motor_PWM_B(Power);
	}
	else 
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);
		Motor_PWM_B(MaxResponse-(-Power));
	}
}

void Motor_C_Run(int Power)
{
	if(Power >= MaxResponse)
		Power = MaxResponse;
	if(Power <= - MaxResponse)
		Power = - MaxResponse;
	
	if(Power >= 0)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);
		Motor_PWM_C(Power);
	}
	else 
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);
		Motor_PWM_C(MaxResponse-(-Power));
	}
}

int Map(double value,int PWM)
{
	int temp;
	if( value >=0) 
	{
		temp = (value/PWM)*(PWM - Deadband) + Deadband;
	}
	else 
	{
		temp = (value/PWM)*(PWM - Deadband) - Deadband;
	}
	return temp;
}

