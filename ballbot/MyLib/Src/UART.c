#include "UART.h"

extern UART_HandleTypeDef huart6;

/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART and Loop until the end of transmission */
  HAL_UART_Transmit(&huart6, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}

float move = 0.0f;
uint8_t reset_en = 0;

extern uint8_t mode;

extern UART_HandleTypeDef huart6;
extern PIDType PIDAngleX, PIDAngleY;
extern PIDType PIDMotor1, PIDMotor2, PIDMotor3;
extern uint8_t Deadband;
extern uint8_t Tran;
extern uint8_t Rcv;

float roll_uart = 0.0f, pitch_uart = 0.0f;

uint8_t uart1_rx_buffer[UART1_RX_BUFFER_SIZE];
uint8_t uart1_rx_index = 0;

uint8_t PID_Data[PID_DATA];

uint8_t RxDebugGetByte;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == huart6.Instance)	
	{
		if(uart1_rx_index == 0)
		{
			ClearUart1RxBuffer();
		}
		if(RxDebugGetByte != 'l')
		{
			uart1_rx_buffer[uart1_rx_index++] = RxDebugGetByte;
		}
		else
		{
			uart1_rx_index = 0;
			Tran = 1;
		}
			
		HAL_UART_Receive_IT(&huart6, &RxDebugGetByte, 1);
	
		//HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
	}
}

void ClearUart1RxIndex(void)
{
	uart1_rx_index = 0;
}

void ClearUart1RxBuffer(void)
{
	for (uint16_t i=0; i < UART1_RX_BUFFER_SIZE; i++)
	{
		uart1_rx_buffer[i] =0;
	}
	uart1_rx_index = 0;
}

void PID_Parameter(void)
 {   
	 uint8_t  strkp1[10] = {0}, strki1[10] = {0}, strkd1[10] = {0}, 
						strkp2[10] = {0}, strki2[10] = {0}, strkd2[10] = {0}, 
						strkp3[10] = {0}, strki3[10] = {0}, strkd3[10] = {0},
						strkp4[10] = {0}, strki4[10] = {0}, strkd4[10] = {0},
						strkp5[10] = {0}, strki5[10] = {0}, strkd5[10] = {0},
						strmove_up[10]= {0}, strmove_do[10]= {0}, strmove_tr[10]= {0}, strmove_ph[10]= {0},
						strDeadBand[10] = {0}, strRollOff[10] = {0}, strPitchOff[10] = {0};

		 if(PID_Data[0] == 'P' && PID_Data[1] == 'I' && PID_Data[2] == 'D' && PID_Data[3] == '1')
		 {
			 String_Split(PID_Data,',',1,strkp1);// stringin, character,starpos, stringout. sau khi gap dau phay thu nhat thi string in gan vao kp
			 String_Split(PID_Data,',',2,strki1);
			 String_Split(PID_Data,',',3,strkd1);
			 
			 PIDAngleX.Kp = Str2Double(strkp1);
			 PIDAngleX.Ki = Str2Double(strki1);
			 PIDAngleX.Kd = Str2Double(strkd1);
		 }
		 
		 if(PID_Data[0] == 'P' && PID_Data[1] == 'I' && PID_Data[2] == 'D' && PID_Data[3] == '2' )
		 {
			 String_Split(PID_Data,',',1,strkp2);// stringin, character,starpos, stringout. sau khi gap dau phay thu nhat thi string in gan vao kp
			 String_Split(PID_Data,',',2,strki2);
			 String_Split(PID_Data,',',3,strkd2);
			 
			 PIDAngleY.Kp = Str2Double(strkp2);
			 PIDAngleY.Ki = Str2Double(strki2);
			 PIDAngleY.Kd = Str2Double(strkd2); 
		 }
		 
		 if(PID_Data[0] == 'P' && PID_Data[1] == 'I' && PID_Data[2] == 'D' && PID_Data[3] == '3')
		 {
			 String_Split(PID_Data,',',1,strkp3);// stringin, character,starpos, stringout. sau khi gap dau phay thu nhat thi string in gan vao kp
			 String_Split(PID_Data,',',2,strki3);
			 String_Split(PID_Data,',',3,strkd3);
			 
			 PIDMotor1.Kp = Str2Double(strkp3);
			 PIDMotor1.Ki = Str2Double(strki3);
			 PIDMotor1.Kd = Str2Double(strkd3);
		 }
		 
		 if(PID_Data[0] == 'P' && PID_Data[1] == 'I' && PID_Data[2] == 'D'&& PID_Data[3] == '4')
		 {
			 String_Split(PID_Data,',',1,strkp4);// stringin, character,starpos, stringout. sau khi gap dau phay thu nhat thi string in gan vao kp
			 String_Split(PID_Data,',',2,strki4);
			 String_Split(PID_Data,',',3,strkd4);
			 
			 PIDMotor2.Kp = Str2Double(strkp4);
			 PIDMotor2.Ki = Str2Double(strki4);
			 PIDMotor2.Kd = Str2Double(strkd4);

		 }
		 
		 if(PID_Data[0] == 'P' && PID_Data[1] == 'I' && PID_Data[2] == 'D'&& PID_Data[3] == '5')
		 {
			 String_Split(PID_Data,',',1,strkp5);// stringin, character,starpos, stringout. sau khi gap dau phay thu nhat thi string in gan vao kp
			 String_Split(PID_Data,',',2,strki5);
			 String_Split(PID_Data,',',3,strkd5);
			 
			 PIDMotor3.Kp = Str2Double(strkp5);
			 PIDMotor3.Ki = Str2Double(strki5);
			 PIDMotor3.Kd = Str2Double(strkd5);
		 }
		 
		 if(PID_Data[0] == 'S')
		 {
			 String_Split(PID_Data,',',1,strRollOff);// stringin, character,starpos, stringout. sau khi gap dau phay thu nhat thi string in gan vao kp
			 String_Split(PID_Data,',',2,strPitchOff);
			 String_Split(PID_Data,',',3,strDeadBand);
			 Deadband = Str2Double(strDeadBand);
			 roll_uart = Str2Double(strRollOff);
			 pitch_uart = Str2Double(strPitchOff);
		 }
		 
		 if(PID_Data[0] == 'r')
		 {
			 Rcv = 1;
		 }
		 
		 if(PID_Data[0] == 's')
			{
				Rcv = 0;
			}
		 if(PID_Data[0] == 'u' && PID_Data[1] == 'p' )
		 {
			 String_Split(PID_Data,',',1,strmove_up);
			 move = Str2Double(strmove_up);
			 reset_en = 1;
			 mode = 1;	
		 }
		 if(PID_Data[0] == 'd' && PID_Data[1] == 'o' )
		 {
			 String_Split(PID_Data,',',1,strmove_do);
			 move = Str2Double(strmove_do);
			 reset_en = 1;
			 mode = 2;
		 }
		 
		 if(PID_Data[0] == 't' && PID_Data[1] == 'r' )
		 {
			 String_Split(PID_Data,',',1,strmove_tr);
			 move = Str2Double(strmove_tr);
			 reset_en = 1;
			 mode = 3;
		 }
		 
		 if(PID_Data[0] == 'p' && PID_Data[1] == 'h' )
		 {
			 String_Split(PID_Data,',',1,strmove_ph);
			 move = Str2Double(strmove_ph);
			 reset_en = 1;
			 mode = 4;
		 }
		 if(PID_Data[0] == 'R' && PID_Data[1] == 'E')
		 {
			 reset_en = 1;
				mode = 0;
		 }
 }


