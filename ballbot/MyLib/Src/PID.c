#include "PID.h"

PIDType PIDMotor1, PIDMotor2, PIDMotor3;
PIDType PIDAngleX, PIDAngleY;

float Pre_AngleX = 0.0f, Pre_pre_AngleX = 0.0f;
float Pre_AngleY = 0.0f, Pre_pre_AngleY = 0.0f;


/*
	Ftimer = Fclk / (Prescaler + 1);
	Ttimer = 1/Ftimer;
	Example:
		Ftimer = (72*10^6)/ (3599 + 1) = 20KHz;
		Ttimer = 1/(20KHz) = 0.05ms;
	need delay 25ms = 0.05ms*500 --> Period = 500 - 1;
	
*/

void PIDMotorA(double SetPoint, double Speed, int MaxResponse)
{
	PIDMotor1.PIDError = SetPoint - Speed;

	PIDMotor1.pPart = PIDMotor1.Kp * PIDMotor1.PIDError;
	PIDMotor1.iPart += PIDMotor1.Ki * PIDMotor1.PIDError;
	PIDMotor1.dPart = PIDMotor1.Kd * (PIDMotor1.PIDError - PIDMotor1.PIDErrorTemp1);

	if (PIDMotor1.iPart > 250)
		PIDMotor1.iPart = 250;
	else if (PIDMotor1.iPart < -250)
		PIDMotor1.iPart = -250;

	PIDMotor1.PIDResult = (PIDMotor1.pPart + PIDMotor1.iPart + PIDMotor1.dPart) ;

	if (fabs(PIDMotor1.PIDResult) > MaxResponse) 
		PIDMotor1.PIDResult = SIGN(PIDMotor1.PIDResult) * MaxResponse;
	
	PIDMotor1.PIDErrorTemp1 = PIDMotor1.PIDError;
}

void PIDMotorB(double SetPoint, double Speed, int MaxResponse)
{
	PIDMotor2.PIDError = SetPoint - Speed;

	PIDMotor2.pPart = PIDMotor2.Kp * PIDMotor2.PIDError;
	PIDMotor2.iPart += PIDMotor2.Ki * PIDMotor2.PIDError;
	PIDMotor2.dPart = PIDMotor2.Kd * (PIDMotor2.PIDError - PIDMotor2.PIDErrorTemp1);

	if (PIDMotor2.iPart > 250)
		PIDMotor2.iPart = 250;
	else if (PIDMotor2.iPart < -250)
		PIDMotor2.iPart = -250;

	PIDMotor2.PIDResult = (PIDMotor2.pPart + PIDMotor2.iPart + PIDMotor2.dPart) ;

	if (fabs(PIDMotor2.PIDResult) > MaxResponse) 
		PIDMotor2.PIDResult = SIGN(PIDMotor2.PIDResult) * MaxResponse;
	
	PIDMotor2.PIDErrorTemp1 = PIDMotor2.PIDError;
}

void PIDMotorC(double SetPoint, double Speed, int MaxResponse)
{
	PIDMotor3.PIDError = SetPoint - Speed;

	PIDMotor3.pPart = PIDMotor3.Kp * PIDMotor3.PIDError;
	PIDMotor3.iPart += PIDMotor3.Ki * PIDMotor3.PIDError;
	PIDMotor3.dPart = PIDMotor3.Kd * (PIDMotor3.PIDError - PIDMotor3.PIDErrorTemp1);

	if (PIDMotor3.iPart > 250)
		PIDMotor3.iPart = 250;
	else if (PIDMotor3.iPart < -250)
		PIDMotor3.iPart = -250;

	PIDMotor3.PIDResult = (PIDMotor3.pPart + PIDMotor3.iPart + PIDMotor3.dPart) ;

	if (fabs(PIDMotor3.PIDResult) > MaxResponse) 
		PIDMotor3.PIDResult = SIGN(PIDMotor3.PIDResult) * MaxResponse;
	
	PIDMotor3.PIDErrorTemp1 = PIDMotor3.PIDError;
}

void PIDVx(float Angle, int MaxResponse)
{
//	PIDAngleX.PIDError = SetAngle - Angle;
//	PIDAngleX.pPart = PIDAngleX.Kp*PIDAngleX.PIDError;
//	PIDAngleX.iPart += PIDAngleX.Ki*PIDAngleX.PIDError;
//	PIDAngleX.dPart = PIDAngleX.Kd*(PIDAngleX.PIDError - PIDAngleX.PIDErrorTemp1);
	
	PIDAngleX.pPart = PIDAngleX.Kp*(Angle - Pre_AngleX);
	PIDAngleX.iPart = PIDAngleX.Ki*Angle;
	PIDAngleX.dPart = PIDAngleX.Kd*(Angle - 2*Pre_AngleX + Pre_pre_AngleX);
	
	PIDAngleX.PIDResult += PIDAngleX.pPart + PIDAngleX.iPart + PIDAngleX.dPart;
	
//	if (PIDAngleX.PIDResult > MaxResponse)
//		PIDAngleX.PIDResult = (double)MaxResponse;
//	if (PIDAngleX.PIDResult < -1 * MaxResponse)
//		PIDAngleX.PIDResult = (double)(-1 * (MaxResponse));
//	PIDAngleX.PIDErrorTemp1 = PIDAngleX.PIDError;
	
	if (fabs(PIDAngleX.PIDResult) > MaxResponse) 
		PIDAngleX.PIDResult = SIGN(PIDAngleX.PIDResult) * MaxResponse;	
	
	Pre_pre_AngleX = Pre_AngleX;
	Pre_AngleX = Angle;

}

void PIDVy(float Angle, int MaxResponse)
{
//	PIDAngleY.PIDError = SetAngle - Angle;
//	PIDAngleY.pPart = PIDAngleY.Kp*PIDAngleY.PIDError;
//	PIDAngleY.iPart += PIDAngleY.Ki*PIDAngleY.PIDError;
//	PIDAngleY.dPart = PIDAngleY.Kd*(PIDAngleY.PIDError - PIDAngleY.PIDErrorTemp1);
	PIDAngleY.pPart = PIDAngleY.Kp*(Angle - Pre_AngleY);
	PIDAngleY.iPart = PIDAngleY.Ki*Angle;
	PIDAngleY.dPart = PIDAngleY.Kd*(Angle - 2*Pre_AngleY + Pre_pre_AngleY);
	
	PIDAngleY.PIDResult += PIDAngleY.pPart + PIDAngleY.iPart + PIDAngleY.dPart;
	
//	if (PIDAngleY.PIDResult > MaxResponse)
//		PIDAngleY.PIDResult = (double)MaxResponse;
//	if (PIDAngleY.PIDResult < -1 * MaxResponse)
//		PIDAngleY.PIDResult = (double)(-1 * (MaxResponse));
//	PIDAngleY.PIDErrorTemp1 = PIDAngleY.PIDError;
	
	if (fabs(PIDAngleY.PIDResult) > MaxResponse) 
		PIDAngleY.PIDResult = SIGN(PIDAngleY.PIDResult) * MaxResponse;	
	
	Pre_pre_AngleY = Pre_AngleY;	
	Pre_AngleY = Angle;

}



